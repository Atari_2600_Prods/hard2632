
CC = gcc -Os -Wall -pedantic
DASM = dasm
STELLA = stella
QRENCODE = qrencode -8
HEXDUMP = hexdump -e '"%02.2_ax:" 4/1 " %02x" "\n"'

BASENAMES = $(sort $(basename $(wildcard *.asm)))

BIN_2KS   = $(addsuffix _2k.bin,$(BASENAMES))
BINS      = $(addsuffix .bin,$(BASENAMES))
QRS       = $(addsuffix .png,$(BASENAMES))
HDS       = $(addsuffix .hd,$(BASENAMES))

ALL = $(BINS) $(BIN_2KS) $(HDS) $(QRS)

all: $(ALL)

clean:
	rm -f $(ALL) dump2632

dump2632: dump2632.c

%.run: %_2k.bin
	$(STELLA) $<

%_2k.bin: %.bin
	for i in $$(seq 0 63); do cat $< ; done >$@

%.bin: %.asm
	$(DASM) $< -f3 -o$@.tmp
	{ OFFSET=$$({ grep '^OFFSET *=' $< || echo =0 ;}|cut -f2 -d=|tr -d ' ') ;\
	dd bs=1 if=$@.tmp  skip=$${OFFSET} ;\
	dd bs=1 if=$@.tmp count=$${OFFSET} ;\
	} > $@
	rm -f $@.tmp

%.png: %.bin
	$(QRENCODE) -o $@ <$<
	
%.hd: %.bin dump2632
	./dump2632 <$< >$@

