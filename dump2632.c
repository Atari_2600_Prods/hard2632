
#include <stdio.h>

unsigned int bin2dcb( unsigned char val )
{
   unsigned int retval = 0;
   unsigned int pos = 1;
   int i;

   for( i = 0; i < 8; ++i )
   {
      retval += pos * (val & 1);
      pos *= 10;
      val >>= 1;
   }

   return retval;
}
 
int main( int argc, char *argv[] )
{
   FILE *in  = stdin;
   FILE *out = stdout;
   unsigned char buffer[0x20];
   int o;

   fread( buffer, sizeof(buffer), 1, in );
   fprintf( out, "hex 00 08 10 18 | binary for switches\n" );
   fprintf( out, "=====================================================\n" );
   for( o = 0; o < 8; ++o )
   {
      fprintf( out, "+%x: %02x %02x %02x %02x | %08u %08u %08u %08u\n", o,
                    buffer[o], buffer[o+8], buffer[o+16], buffer[o+24],
                    bin2dcb(buffer[o]), bin2dcb(buffer[o+8]),
                    bin2dcb(buffer[o+16]), bin2dcb(buffer[o+24]) );
   }
   
   return 0;
}

